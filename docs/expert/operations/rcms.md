# RCMS

## MiniDAQ

In addition to the standard RCMS FM interface, a special set of
applications and infrastructure is provided by the cDAQ group to mimic
as closely as possible, cDAQ operations. This infrastructure is known as
"MiniDAQ". The GEM MiniDAQ configuration by default contains a DAQ
function manager and a TCDS function manager.

### Using the automator

The "automator" configuration allows you to create a run and get to
any state provided all intermediate states transitioned successfully.

### Add/remove FED

To add or remove a FED from the run, use the `FED&TTS` button. Here you
must select the FED, whether the DAQ is enabled/disabled, and whether
the TTS is enabled/disabled.

### Set random trigger rate

Changing the random trigger rate is done in the `LPMController`
`application<gemos-tcds-lpm-controller>`{.interpreted-text role="ref"}.

### Select CMSSW release (HLT key)

Use the `HLT` selector to choose the HLT menu as well as the appropriate
CMSSW release to be used.

### Enable/disable T0 transfer

If the data being taken should be written out to the tier 0 (T0), make
sure to set the `T0_TRANSFER_ON`. If this is not set at the time of data
taking, provided the DAQ FM is not dropping events at the BU, a manual
transfer can be requested after the fact, provided not too much time has
passed.

### MiniDAQ stress-test

One of the main uses of MiniDAQ is to emulate the conditions used during
cDAQ operations when validating new software and firmware. Setting up
for a "stress-test" involves configuring a normal run. Several
subsequent steps may involve:

-   cycling through many start/stop transitions
-   setting a high random trigger rate (>100kHz)
-   enabling standard CMS B-go sequences (`HardReset`, `Resync`)

!!! note
    If the random rate is too high, due to the limitations of the hardware
    availble in the current MiniDAQ, errors may be encountered simply
    because the disk writing is too slow. To isolate issues solely related
    to the GEM software and firmware, it may be useful to disable the DAQ
    FM, or tell the DAQ FM to "drop events at BU".

## RS manager

The Resource Service Manager (RS Manager) is a tool provided by the CMS
RCMS developers to interface with the RS database (RSDB). This database
hosts the RCMS run configurations.

### Obtaining the RS Manager

The RSManager application is provided with the RCMS releases. Releases
are announced on the `hn-cms-run-control` hypernews, and instructions to
download are located on the [CMS run
control](http://cmsdoc.cern.ch/cms/TRIDAS/RCMS/Downloads/downloadsIndex.html)
website.

### Using the RS Manager

Using the RS manager requires Java. You should also ensure that you will
be able to communicate with the databases the RS manager will attempt to
manipulate. A configuration file can be defined if there are several DBs
you routinely work with, and you can switch them on the fly in the tool.

### xDAQ configurations in RSDB

The RCMS team suggest that the subsystem xDAQ executives assign port
numbers starting from the base FM port + 100, e.g., at P5, where the
`gempro` FM runs at `cms-gempro.cms:20000/rcms`, the first GEM xDAQ
executive should be assigned a port `20100`. Various RCMS services may
be allocated to ports below this, but there should not be any assigned
above this.

