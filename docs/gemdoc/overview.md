# Overview of GEM DOC Responsibilities 

## Monitor the status of CMS and the LHC 

As DOC, it is important to keep an eye on these pages in addition to monitoring GE1/1 and the GE2/1 Demonstrator.  

1. [LHC status](https://op-webtools.web.cern.ch/vistar/vistars.php)  : What is the beam status? 
    * the comment box is also useful to see when the next stable beams will occur, how many bunches, etc. 


2. [Central DCS](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/dcs) : Is GEM in central DCS? Are we “Ready for Physics”?

3. [DAQ](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/daq) : Is GEM in global DAQ? What’s the current Run #? How long has the current run been going? 

4. [Online DQM](https://cmsweb.cern.ch/dqm/online) : keep an eye on the summary plot. Does our data quality look good? ( <  6 chambers in error per endcap)


5. [OMS](https://cmsoms.cern.ch/) : monitoring tool for Run Summary. Clicking the Run Range will show you useful info about the run (time, beam mode, which components are out) 
    * it's helpful to keep track of CSC because we depend on them for trigger readout. 

    * Also important! Keep an eye on the time - is it UTC or local? (to change to local, unclick the checkbox at the bottom labeled UTC time) 

--- 
## Reporting in Google Docs 

1. [Run List 2024](https://docs.google.com/spreadsheets/d/1md0qlCU7T7Isaa5_e6CAzrdJ3DQdap-T5H1eUQ4rvmE/edit?gid=370741251#gid=370741251)  : this is where you report the global runs (> 1hr) with info you find using the OMS page + **any useful info about GE11 and GE21** (this is mainly to report on the LV and HV status).  
    *  Runs are separated by month. There is a separate page to report local runs. 

2. [ToBeDone](https://docs.google.com/spreadsheets/u/1/d/1m_OqvUmCpz6ge8rljOpFvAVRUmRCXPSGtEvBphsajBo/htmlview?pli=1#) : Important here is a) the list of HV anomalies and Shorts and b) the Equivalent divider current to HV values
    * When there is a change in HV anomalies and/or shorts, this must be updated. 

--- 

## Communication Responsibilities

1. [Elogs](https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/elog?_afrMFO=0&_afrMFR=192&_afrMFS=0&_afrWindowMode=0&_afrMFG=0&_afrMFCI=0&_afrMFH=685&_afrMFM=0&_afrMFDH=900&_afrMFC=10&_afrMT=screen&_afrLoop=14167596648366733&_afrFS=16&_afrMFW=1440&Adf-Window-Id=ia1u8l0gr&_afrMFDW=1440&__adfpwp_action_portlet=683379043&__adfpwp_backurl=https://cmsonline.cern.ch:443/webcenter/portal/cmsonline/pages_common/elog?_afrMFO=0&_afrMFR=192&_afrMFS=0&_afrWindowMode=0&_afrMFG=0&_afrMFCI=0&_afrMFH=685&_afrMFM=0&_afrMFDH=900&_afrMFC=10&_afrMT=screen&_afrLoop=14167596648366733&_afrFS=16&_afrMFW=1440&Adf-Window-Id=ia1u8l0gr&_afrMFDW=1440&__adfpwp_mode.683379043=1&_piref683379043.strutsAction=/viewSubcatMessages.do?catId=1691&subId=1715&page=1)  : this is for official reporting (usually the doc writes elogs in the section  GEM/P5/Commissioning)

2. [Run Coordination Meeting](https://schwick.web.cern.ch/schwick/rctools/) : This is where you fill out minutes for the daily RC meeting at 9:30. Write your report by clicking the pencil under DOC reports. Also review the full report by clicking on the orange figure reading under Daily Report. It is recommended to fill out this report at least 30 minutes before the meeting begins.  

3. [Mattermost](https://mattermost.web.cern.ch/cms-gem-ops/channels/gem-weekly-operations) : Primary source of communication between DOC and experts; usually in the Weekly Operations channel, sometimes in DCS or Commissioning. However, if there is a question that needs to be **IMMEDIATELY** answered, better to call the relevant expert. 

4. In general, the DOC is the link between GEM Experts and the Central Shifters. You need to be aware of the plans for GEM and how that fits in with the schedule for CMS and report to both sides. 


    ### Common Elogs 

    **LHC Fill** 

    Go to the [Overview](https://cmsgemonline-monitoring.app.cern.ch/d/vH594Q2Vz/overview?orgId=1&refresh=30s) page in grafana and looking at how many VFATs Enabled. 
    A long straight vertical line is indication of the system being reconfigured
    Unstable GBT == loss of vfats is expected 

    Report in an elog % of VFATs enabled throughout the entire fill 
    
    * FED 1467 : # / 1728

	* FED 1468 : # / 1728 

	* FED 1469 : # / 48 

    Include only the longest couple of runs within a fill along with the corresponding summary plots from the Online DQM page.
    >Example elog : [LHC Fill Elog](http://cmsonline.cern.ch/cms-elog/1182556)


    **Calibration Scans** 

    DOCs are asked to take daily calibration scans to monitor the GE1/1 detector. (More information about these scans in the DAQ Guide)
    >Example elog : [Calibration Scan Elog](http://cmsonline.cern.ch/cms-elog/1187322) 


    **Other** 

    Anytime something is changed in the DCS, or a problem occurs, an elog must be written to keep accurate records of operations at P5. 

