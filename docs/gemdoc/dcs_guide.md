# A GEM DOC's DCS Guide 

This guide is to help walk new docs through basic procedures at p5 concerning the DCS. 

## Taking Control of the FSM 
Make sure you are logged in to the DCS before you ask the technical shifter to take local control of the FSM 

**Reasons for taking control of the FSM** : You need to interact with

1. the upper left grey box on the DCS 

2. the tree panel on the left 

**Example situations include**  

1. Including/Excluding a SC 
2. Turning ON/OFF/HV to Standby a specific chamber, select chambers, or entire endcap through the FSM 
    - When taking local control from central, wait a good 10 seconds before taking it (it will blink yellow ``` "NOBODY controls the system" ```) 
  

**Manually Recovering HV for a Chamber (while in Local)**

When a chamber trips, and is unable to automatically recover due to exceeding its max. recovery times per day, it is up to the doc to manually recover the chamber. 
If you take control or have control of the FSM, this is the safer method of recovering a chamber because you do not have to play around with the I0. 


1. Clear Alarms 
    - click on CLR ALARMS (at the top)
    - click on clear all HV alarms in mainframe 
2. Find the chamber in the leftmost part of the DCS underneath your login (``` CMS_GEM -> GEM_Logical -> GEM_ENDCAP_* ```) 
    Right click on : ```GE11/*/*``` and choose "Show in new tab"; Here you can apply the usual FSM commands to 1 layer or both 
3. Go To STANDBY HV ON for chamber(s) with trips in the case that you have multi-channel trips. If you only have 1 channel that trips, just go straight to 4 
4. Power ON HV for chamber(s) with trip(s)
        - watch the ramp up 
5. If everything goes well, and the ramp up is successful, the FSM will restore the I0 back 

---

## Evidence of a short or HV anomaly 
If Imon goes up (past 20uA) and thus trips again during ramp up, it is necessary to change the recipe settings for the specific chamber, channel.

**To distinguish between a short or HV anomaly :** 

* Short : Vmon / Imon = 10.0
* HV anomaly : Vmon / Imon < 10.0 

**To change the recipe settings :** 

1. Click on Recipe Settings located at the top menu in the DCS 
2. Choose the chamber you want 
3. Select a higher I0 value for the ramp up (iSafeLimit) and for after a ramp up (iSetOn and iSetStb). Usually iSetStb = iSetOn. You will have to fill out the I0 values for all channels, not just the one that tripped. 
4. Double check that the new Recipe Limits have been applied by clicking "...." 
5. Switch HV back on through the FSM 
6. Make sure to write an elog and notify! 

--- 
## Manually Recovering Chamber(s) without the FSM 

One can also recover a tripped chamber without the FSM. However, only do this if you are knowledgeable about the procedure. *Follow the general rule that anytime a channel is ramped up, I0 = ISafeLimit + 10*. This procedure is therefore slightly more complicated for SC with short(s)/hv anomaly(ies).

1. Clear Alarms 
    - click on CLR ALARMS (at the top) 
    - click on clear all HV alarms in mainframe 
2. Click on the SC that tripped (Ly1 or Ly2 NOT the HV board) 
3. If one electrode tripped, keep in FREE Mode. If more than one tripped, switch to GEM mode; apply settings 
4. Set ISafeLimit + 10 to the SC (just a simple 20.0uA for all healthy SC) and apply settings. 
    - watch the ramp up carefully (especially Imon) 
5. If everything goes well, and the ramp up is successful, restore I0 to iSetOn (for healthy SC I0 = 10.0uA) 


**Again**, I note that ramping up a SC through the FSM is the safer, more common practice -- especially for SC with shorts and or hv anomalies. FSM recipe automatically applies the correct I0 values for both ramp up and after. 

---

## Turning OFF the Induction Gap (G3Bot) 

This configuration is used when taking sbit scans. If you aren't already in local FSM, ask for control from the technical shifter. 

1. Select Settings 
2. Select Chamber LV/HV settings
3. Select all Chambers (you will have to repeat for the other endcap and GE21)
4. Switch ALL ON 
5. Click on the tick box above V column 
6. Turn OFF the Induction Gap (G3Bot)
7. Apply HV settings 
8. Wait for ramp down 
9. Check that the induction gap is off in the overview panel in the lower left corner and clicking on the GE21 Demonstrator Layers to view the quick HV status. 
10. When ready to turn G3Bot back on, simply use the FSM and apply GO_To_STANDBY, then HV ON.

---
## GEM Magnet Operations 

Whenever there is a change in the CMS magnetic field, the magnet protection should be triggered 
After Magnet Field is stable (at **T or at 0T ) it is necessary to do HV training : 

1. ONLY G1Top @ 420V (1hr)
2. ONLY G2Top @ 420V (1hr) 
3. ONLY G3Top @ 380V – lower for protection of electronics (1hr)

4. All Foils (Tops) ON @ above values (1hr) 
5. HV Standby – to check stability of the system (~ 30min) 
6. HV ON & rejoin global 

    * check beforehand what exact procedure to follow due to time concerns 
    * Make sure to turn OFF whatever was ON before proceeding to the next step (i.e. G1Top ON, G1Top OFF & then G2Top ON) 

This procedure is done using Chamber LV/HV settings.  

---
## Turning ON/OFF PConv 

For times like 'January' Mode when LV, HV needs to be OFF & racks in UXC need to be OFF during the night, follow below steps. 
    LV, HV can be turned OFF through the FSM. However, for the racks in UXC : 

1. Go to Settings, CAEN Settings 
2. To turn ON : *Double Click* on Channel1 [2,4,6,8] then turn ON Channel0 
3. Next, turn on Channel 1 and 0 for [1,3,5,7] 
    - sometimes one of the two won't turn on. Just try the other channel in the same PConv and they both will turn on 

Just think when turning ON : First fix BLUE and then the one in ERROR/RED will go to BLUE 

4. To turn OFF : go in opposite direction of turning ON 
5. Turn OFF Channel0 [1,3,5,7] & then Channel1 should go off - just check that it is off 
6. Turn OFF Channel0 [2,4,6,8] & then Channel1 (LV should go RED in [1,3,5,7])

The IMPORTANT thing is to turn OFF Channel1 for [2,4,6,8] at the END 

> **Turn OFF Example** : PConv1 Channel0, Channel1; then PConv2 Channel0, Channel1 -> at this point, PConv1 Channel0,1 will turn RED. (repeat for other PConv)

        
