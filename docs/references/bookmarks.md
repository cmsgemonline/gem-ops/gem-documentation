# Bookmarks

## Useful pages at p5

* [GEM RCMS instance](http://cmsrc-gem.cms:20000/rcms/)
* [GEM MiniDAQ online DQM](http://dqmfu-c2b03-46-01.cms:8100/dqm/gem-online/)

### Control applications

* Global
    * [Supervisor](http://srv-s2g18-33-01.cms:20100/urn:xdaq-application:service=supervisor)
    * [Calibration suite](http://srv-s2g18-33-01.cms:20100/urn:xdaq-application:service=calibration)
* MiniDAQ
    * [Supervisor](http://srv-s2g18-33-01.cms:20400/urn:xdaq-application:service=supervisor)
    * [Calibration suite](http://srv-s2g18-33-01.cms:20400/urn:xdaq-application:service=calibration)
* Local
    * [Supervisor](http://srv-s2g18-33-01.cms:20200/urn:xdaq-application:service=supervisor)
    * [Calibration suite](http://srv-s2g18-33-01.cms:20200/urn:xdaq-application:service=calibration)

### Monitoring tools

* GEM- (GE-1/1)
    * [FED1467](http://srv-s2g18-33-01.cms:20300/urn:xdaq-application:service=fed1467-monitor)
* GEM+ (GE+1/1)
    * [FED1468](http://srv-s2g18-33-01.cms:20300/urn:xdaq-application:service=fed1468-monitor)
* GEMPILOT (GE+2/1)
    * [FED1469](http://srv-s2g18-33-01.cms:20300/urn:xdaq-application:service=fed1469-monitor)
