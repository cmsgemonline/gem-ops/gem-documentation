# Slow Control ASIC (SCA)

## The `sca.py` Tool

The `sca.py` is a command line tool for sending a variety of commands to
the SCA. For a description of the possible commands and their calling
syntax execute `sca.py -h` for more information.

## Issuing an SCA Reset

To issue an SCA reset execute the following from the DAQ PC:

``` bash
sca.py r cardName ohMask
```

For example:

``` bash
sca.py r eagle60 0x3
```

This will issue an SCA reset to OH\'s 0 and 1 on `eagle60`.

If a red error message appears for one or more of the OH\'s in your
`ohMask` re-issue the SCA reset until no red error messages appear. For
subsequent SCA resets issued in this way you can use the same `ohMask`
or modify it to remove the healthy OH\'s. If continuing to issue an sca
reset does not resolve the issue (i.e., red error messages continue to
appear) there is a problem and you probably lost communication. In this
case check the status of `GBT0` on each of the problem GBTs using
instructions under `gemos-gbt-ready-registers`{.interpreted-text
role="ref"}, if GBTX is either not ready or was not ready you may need
to either issue a GBTx link reset (see
`gemos-gbt-link-reset`{.interpreted-text role="ref"}), re-program GBT0
(see `gemos-gbt-programming`{.interpreted-text role="ref"}), or in rare
cases power cycle and start from scratch.

## Checking SCA Status

There are two registers of great importance for checking SCA
communication. They are:

``` bash
GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY
GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR
```

Each is a 12 bit register with the $N^{th}$ bit corresponding to the
$N^{th}$ optohybrid. In the case of `READY` if the $N^{th}$ bit is
raised high (e.g., it equals 1) it means the link is ready and
communication is most likely stable (although in some cases the READY
bit for a given optohybrid is 1 but slow control is not possible). In
the case of `CRITICAL_ERROR` if the $N^{th}$ bit is raised high (e.g.,
it equals 1) it means the SCA controller on the $N^{th}$ optohybrid has
encountered a critical error and needs an SCA reset.

### Using `amc_info_uhal.py`

You can get the SCA status on all optohybrids on a CTP7 from
`amc_info_uhal.py` command. Execute:

``` bash
amc_info_uhal.py --shelf=X -sY
```

The relevant SCA output for all optohybrids on the CTP7 in slot `Y` of
shelf `X` will look like:

``` bash
--=======================================--
-> GEM SYSTEM SCA INFORMATION
--=======================================--

READY             0x000003fc
CRITICAL_ERROR    0x00000000
NOT_READY_CNT_OH00 0x00000001
NOT_READY_CNT_OH01 0x00000001
NOT_READY_CNT_OH02 0x00000002
NOT_READY_CNT_OH03 0x00000002
NOT_READY_CNT_OH04 0x00000002
NOT_READY_CNT_OH05 0x00000002
NOT_READY_CNT_OH06 0x00000002
NOT_READY_CNT_OH07 0x00000002
NOT_READY_CNT_OH08 0x00000002
NOT_READY_CNT_OH09 0x00000002
NOT_READY_CNT_OH10 0x00000001
NOT_READY_CNT_OH11 0x00000001
```

Note that `ipbus` service must be running on the CTP7.

### Using `gem_reg.py`

You can get the SCA status on all optohybrids on a CTP7 from
`gem_reg.py` using the following command, with example output:

``` bash
eagleXX > rwc SCA*READY
0x66c00400 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY                   0x00000002
0x66c00408 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH0       0x00000001
0x66c0040c r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH1       0x00000002
0x66c00410 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH2       0x00000001
0x66c00414 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH3       0x00000001
eagle60 > read GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR
0x66c00404 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR          0x00000000
```

Here we see that SCA READY is `0x2` so only OH1 has stable communication
and no links are in error (critical error is `0x0`).

Note that `rpcsvc` must be running on the CTP7.

## VFAT Reset Lines

In the OHv3c version the VFAT reset lines are controlled by the SCA and
not the OH FPGA. In CTP7 firmware versions higher than 3.5.2 the resets
will automatically be lifted and no user action is required. On older
CTP7 FW versions to lift the VFAT resets first program the OHv3c FPGA
following instructions under Section
`gemos-optohybrid-programming`{.interpreted-text role="ref"} then
execute:

1.  From the DAQ machine execute
    `sca.py eagleXX ohMask gpio-set-direction 0x0fffff8f`,
2.  From the DAQ machine execute
    `sca.py eagleXX ohMask gpio-set-output 0xf00000f0`,
3.  from the DAQ machine send a GBTx link reset, see Section
    `gemos-gbt-link-reset`{.interpreted-text role="ref"},
4.  Then check the status of the GBT\'s in `gem_reg.py`
    (`kw OH_LINKS.OH0.GBT`),
5.  Report the status of link good of all VFATs (`kw LINK_GOOD 0`),
6.  Report the status of sync error counters of all VFATs
    (`kw SYNC_ERR_CNT 0`),
7.  Check for slow control of al VFATs (`kw CFG_RUN 0`)
